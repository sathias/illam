# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :admin do
      resources :users
      resources :categories
      resources :labours
      resources :materials
      resources :material_types
      resources :projects
      resources :transportations
      resources :vendors
      resources :works
      resources :users_projects

      root to: "projects#index"
  end
  devise_for :users
  root to: 'admin/projects#index'
  #mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

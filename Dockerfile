FROM ruby:2.6.3
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
ENV APP_HOME /construction
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
ADD Gemfile* $APP_HOME/

RUN gem install bundler:2.1.4 && \
    bundle install --without test development --jobs 4

ADD . $APP_HOME
# Start the main process.
CMD ["./run-server.sh"]
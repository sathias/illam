# frozen_string_literal: true

class Category < ApplicationRecord
  belongs_to :project
  has_many :works

  def update_amount
    self.amount = works.sum(&:amount)
    self.construction_amount = works.sum(&:construction_amount)
    save!
  end

  after_save do
    project.update_amount
  end
end

# frozen_string_literal: true

class Work < ApplicationRecord
  has_many :materials
  has_many :vendors
  has_many :labours
  has_many :transportations

  belongs_to :category

  def update_amount
    self.amount = materials.sum(&:amount) + vendors.sum(&:amount) + labours.sum(&:amount) + transportations.sum(&:amount)
    save!
  end

  def update_construction_amount
    self.construction_amount = materials.construction.sum(&:amount) + labours.sum(&:amount)
    save!
  end

  after_save do
    category.update_amount
  end
end

# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  class << self
    def ignore_columns_in_view
      %i[created_at updated_at id].collect(&:to_s)
    end

    def belongs_to_association
      @b_assocs ||= reflect_on_all_associations(:belongs_to).map(&:name)
    end
  end
end

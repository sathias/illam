# frozen_string_literal: true

class Vendor < ApplicationRecord
  belongs_to :work

  after_save do
    work.update_amount
  end
end

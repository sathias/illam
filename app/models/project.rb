# frozen_string_literal: true

class Project < ApplicationRecord
  has_many :categories
  belongs_to :engineer, class_name: 'User', foreign_key: 'engineer_id'
  belongs_to :owner, class_name: 'User', foreign_key: 'owner_id'
  has_many :users_projects
  has_many :users, through: :users_projects

  def update_amount
    self.amount = categories.sum(&:amount)
    self.construction_amount = categories.sum(&:construction_amount)
    save!
  end
end

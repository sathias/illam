# frozen_string_literal: true

class Labour < ApplicationRecord
  belongs_to :work

  after_save do
    work.update_amount
    work.update_construction_amount
  end
end

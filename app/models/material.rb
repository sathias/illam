# frozen_string_literal: true

class Material < ApplicationRecord
  belongs_to :work
  belongs_to :material_type

  scope :construction, -> { where(construction_cost: true) }

  after_save do
    work.update_amount
    work.update_construction_amount
  end
end

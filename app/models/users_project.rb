class UsersProject < ApplicationRecord
  self.table_name = "users_projects"

  belongs_to :user
  belongs_to :project
end

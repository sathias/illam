# frozen_string_literal: true

class MaterialType < ApplicationRecord
  has_many :materials

  def total_costs
  	materials.sum(&:amount)
  end
end

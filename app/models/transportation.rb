# frozen_string_literal: true

class Transportation < ApplicationRecord
  belongs_to :work

  after_save do
    work.update_amount
  end
end

class Ability
  include CanCan::Ability

  def initialize(user)
    can :manage, Project, users: { id: user.id }
    can :manage, User, { admin: true }
    can :manage, UsersProject, user: { admin: true }
    can :manage, Category, project: { users: { id: user.id } }
    can :manage, Work, category: { project: { users: { id: user.id } } }
    can :manage, Material, work: { category: { project: { users: { id: user.id } } } }
    can :manage, Transportation, work: { category: { project: { users: { id: user.id } } } }
    can :manage, Labour, work: { category: { project: { users: { id: user.id } } } }
    can :manage, Vendor, work: { category: { project: { users: { id: user.id } } } }
  end
end

# frozen_string_literal: true

class MaterialsController < ApplicationController
  before_action do
    @resource_klass = Material
  end

  def create_params
    params.require(:material).permit(:name, :work_id, :amount_in_paise)
  end
end

#!/bin/bash

cd $APP_HOME
bundle exec rake assets:clean && bundle exec rake assets:precompile
echo "Starting DB migrate."

bundle exec rake db:migrate

if [[ $? != 0 ]]; then
  echo "Failed to migrate. Exiting."
  exit 1
fi
echo "DB migrate done!"

bundle exec rails server -b 0.0.0.0
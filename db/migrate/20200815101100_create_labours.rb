# frozen_string_literal: true

class CreateLabours < ActiveRecord::Migration[5.2]
  def change
    create_table :labours, id: :uuid do |t|
      t.string :name
      t.float :amount
      t.uuid :work_id

      t.timestamps
    end
  end
end

# frozen_string_literal: true

class CreateWorks < ActiveRecord::Migration[5.2]
  def change
    create_table :works, id: :uuid do |t|
      t.string :name
      t.uuid :category_id

      t.timestamps
    end
  end
end

# frozen_string_literal: true

class AddIgnoreConstructionCost < ActiveRecord::Migration[5.2]
  def change
    add_column :materials, :construction_cost, :boolean, default: true
  end
end

# frozen_string_literal: true

class AddPaidDate < ActiveRecord::Migration[5.2]
  def change
    add_column :materials, :paid_at, :date
    add_column :vendors, :paid_at, :date
    add_column :labours, :paid_at, :date
    add_column :transportations, :paid_at, :date
  end
end

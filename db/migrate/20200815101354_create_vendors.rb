# frozen_string_literal: true

class CreateVendors < ActiveRecord::Migration[5.2]
  def change
    create_table :vendors, id: :uuid do |t|
      t.string :name
      t.float :amount
      t.uuid :work_id

      t.timestamps
    end
  end
end

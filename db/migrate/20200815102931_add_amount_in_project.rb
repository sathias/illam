# frozen_string_literal: true

class AddAmountInProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :amount, :float, default: 0
  end
end

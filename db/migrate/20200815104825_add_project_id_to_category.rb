# frozen_string_literal: true

class AddProjectIdToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :project_id, :uuid
  end
end

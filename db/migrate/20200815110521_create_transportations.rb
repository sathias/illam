# frozen_string_literal: true

class CreateTransportations < ActiveRecord::Migration[5.2]
  def change
    create_table :transportations, id: :uuid do |t|
      t.string :name
      t.float :amount
      t.uuid :work_id

      t.timestamps
    end
    add_column :works, :construction_amount, :float, default: 0
    add_column :categories, :construction_amount, :float, default: 0
    add_column :projects, :construction_amount, :float, default: 0
  end
end

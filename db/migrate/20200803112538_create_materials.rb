# frozen_string_literal: true

class CreateMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :materials, id: :uuid do |t|
      t.string :name, null: false
      t.uuid :work_id, null: false
      t.float :amount, null: false

      t.timestamps
    end
  end
end

# frozen_string_literal: true

class AddAmountInCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :amount, :float, default: 0
  end
end

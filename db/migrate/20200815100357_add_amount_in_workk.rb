# frozen_string_literal: true

class AddAmountInWorkk < ActiveRecord::Migration[5.2]
  def change
    add_column :works, :amount, :float
  end
end

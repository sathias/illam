# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'pgcrypto'
    create_table :projects, id: :uuid do |t|
      t.string :name
      t.float :overall_estimated_budget
      t.uuid :owner_id
      t.uuid :engineer_id

      t.timestamps
    end
  end
end

# frozen_string_literal: true

class CreateMaterialTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :material_types, id: :uuid do |t|
      t.string :name

      t.timestamps
    end
    add_column :materials, :material_type_id, :uuid
  end
end

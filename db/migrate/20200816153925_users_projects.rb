class UsersProjects < ActiveRecord::Migration[5.2]
  def change
     create_table :users_projects, id: :uuid do |t|
       t.uuid :user_id
       t.uuid :project_id
       t.timestamps
     end
  end
end

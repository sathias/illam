# frozen_string_literal: true

class AddDefaultAmountInWork < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:works, :amount, 0)
  end
end
